#!/usr/bin/tcc -run
// De-scripts a shebang script programatically so you can write makefiles to compile tcc scripts.
#include <unistd.h>

int main (){
 char c[8192*4]= {0};
 for (; read (0, c, 1)&&c[0]!=0xa;);
 for (int r; r= read (0, c, 8192*4), r; write (1, c, r));
 return 0;
}
