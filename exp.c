#!/usr/bin/tcc -run
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
char* μ= "Input two values as arguments\n\0";
int main (int ζ, char** ξ){
 if (ζ<3){ write (1, μ, strlen (μ)); return 1;}
 long double α= atof (ξ[1]);
 long double τ= α;
 for (int i = 1; i< atoi (ξ[2]); i++){
  τ*= α;
 }
 char * t= "%Lg\n\0";
 t[2]= ζ==4? ξ[3][0]:'g';
 dprintf (1, t, τ);
 return 0;
}
