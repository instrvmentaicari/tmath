cc=tcc
cf=-march=native -mtune=native -O2
ds=./descript.c # For removing the script shebang to compile tcc scripts 
all:	./add	./mul	./sub	./div	./mod	./fmod ./sqrt ./exp
./add:	./add.c
	$(ds) <add.c >add.temp.c
	$(cc) $(cf) ./add.temp.c -o  ./add
	rm add.temp.c
./mul:	./mul.c
	$(ds) <mul.c >mul.temp.c
	$(cc) $(cf) ./mul.temp.c -o  ./mul
	rm mul.temp.c
./sub:	./sub.c
	$(cc) $(cf) ./sub.c -o  ./sub
./div:	./div.c
	$(cc) $(cf) ./div.c -o  ./div
./mod:	./mod.c
	$(cc) $(cf) ./mod.c -o  ./mod -lm
./fmod:	./fmod.c
	$(cc) $(cf) ./fmod.c -o  ./fmod -lm
./sqrt:	./sqrt.c
	$(cc) $(cf) ./sqrt.c -o  ./sqrt -lm
./exp:	./exp.c # Segfaults on gcc but not tcc.
	$(ds) <exp.c >exp.temp.c
	$(cc) $(cf) ./exp.temp.c -o  ./exp
	rm exp.temp.c
af=-S -masm=intel
./asm:	./add.c	./mul.c	./div.c	./sub.c	./mod.c	./fmod.c ./sqrt.c ./exp.c
	$(cc) $(cf) ./add.c $(af) -o ./add.s
	$(cc) $(cf) ./mul.c $(af) -o ./mul.s
	$(cc) $(cf) ./sub.c $(af) -o ./sub.s
	$(cc) $(cf) ./div.c $(af) -o ./div.s
	$(cc) $(cf) ./mod.c $(af) -o ./mod.s
	$(cc) $(cf) ./fmod.c $(af) -o ./fmod.s
	$(cc) $(cf) ./sqrt.c $(af) -o ./sqrt.s
	$(cc) $(cf) ./exp.c $(af) -o ./exp.s
clean:
	rm ./add
	rm ./mul
	rm ./sub
	rm ./div
	rm ./mod
	rm ./fmod
	rm ./sqrt
	rm ./exp
cleanasm:
	rm ./add.s
	rm ./mul.s
	rm ./sub.s
	rm ./div.s
	rm ./mod.s
	rm ./fmod.s
	rm ./sqrt.s
	rm ./exp.s
install:	./add	./mul	./sub	./div	./mod	./fmod ./sqrt ./exp
	sudo cp ./add /usr/bin/
	sudo cp ./mul /usr/bin/
	sudo cp ./sub /usr/bin/
	sudo cp ./div /usr/bin/
	sudo cp ./mod /usr/bin/
	sudo cp ./fmod /usr/bin/
	sudo cp ./sqrt /usr/bin/
	sudo cp ./exp /usr/bin/
uninstall:
	sudo rm	/usr/bin/add
	sudo rm	/usr/bin/mul
	sudo rm	/usr/bin/sub
	sudo rm	/usr/bin/div
	sudo rm	/usr/bin/mod
	sudo rm	/usr/bin/fmod
	sudo rm	/usr/bin/sqrt
	sudo rm	/usr/bin/exp
