#!/usr/bin/tcc -run
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
char* μ= "Input two values as arguments\n\0";
int main (int ζ, char** ξ){
 if (ζ<3){ write (1, μ, strlen (μ)); return 1;}
 double α= atof (ξ[1]), β= atof (ξ[2]);
 long int κ= roundf (α), τ= roundf (β);
 dprintf (1, "%lli\n", κ%τ);
 return 0;
}
